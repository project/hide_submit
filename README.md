# Hide Submit Button module

Hide submit module provides a way to hide the submit button in forms after
it has been clicked. This helps to prevent duplicate postings from people who
accidentally double click (or triple click) on a submit button.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/hide_submit).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/hide_submit).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module has no additional requirements other than the Core system module.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-modules).


## Configuration

- Go to _/admin/modules_ and enable the module.
- Go to _/admin/config/content/hide-submit_.
- Configure as needed.
- Save the configuration.
- For user roles that should bypass this module's functionality, there is a
'bypass hide submit' permission.


## Maintainers

- Aaron Ferris - [aaron.ferris](https://www.drupal.org/u/aaronferris)
- Greg Knaddison -[greggles](https://www.drupal.org/u/greggles)
- optalgin - [optalgin](https://www.drupal.org/u/optalgin)
- Dinesh Kumar Sarangapani - [dinesh-kumar-sarangapani](https://www.drupal.org/u/dinesh-kumar-sarangapani)
