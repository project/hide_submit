<?php

namespace Drupal\hide_submit\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Provides a service to alter forms with hide submit functionality.
 */
class HideSubmitFormAlter {
  use StringTranslationTrait;

  /**
   * Current user, used for permission checking.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * String translation service.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $stringTranslation;

  /**
   * The admin context.
   *
   * @var \Drupal\Core\Routing\AdminContext
   */
  protected $adminContext;

  /**
   * The current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * Constructs a new HideSubmitFormAlter object.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation.
   * @param \Drupal\Core\Routing\AdminContext $admin_context
   *   The admin context.
   * @param \Drupal\Core\Path\CurrentPathStack $current_path
   *   The current path.
   */
  public function __construct(AccountProxyInterface $current_user, ConfigFactoryInterface $config_factory, TranslationInterface $string_translation, AdminContext $admin_context, CurrentPathStack $current_path) {
    $this->currentUser = $current_user;
    $this->configFactory = $config_factory;
    $this->stringTranslation = $string_translation;
    $this->adminContext = $admin_context;
    $this->currentPath = $current_path;
  }

  /**
   * Add hide submit functionality to forms.
   *
   * @param array $form
   *   The form to alter.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The state of the form.
   * @param string $form_id
   *   The ID of the form.
   */
  public function alterForm(&$form, FormStateInterface $form_state, $form_id) {
    if ($this->currentUser->hasPermission('bypass hide submit') && $this->currentUser->id() != 1) {
      return;
    }

    $config = $this->configFactory->get('hide_submit.settings');
    if ($config->get('bypass_user_1') && $this->currentUser->id() == 1) {
      return;
    }

    if ($config->get('bypass_admin_routes') && $this->adminContext->isAdminRoute()) {
      return;
    }

    // Configured by paths.
    $configured_urls = $config->get('hide_submit_paths');
    $configured_by_path = $config->get('hide_submit_by_paths_enable');
    if ($configured_by_path) {
      $enabled_or_disabled = $config->get('hide_submit_paths_enabled_disabled');
      $configured_urls = explode("\r\n", $configured_urls);
      $current_path = $this->currentPath->getPath();

      if (!empty($enabled_or_disabled)) {
        // Only use hide submit on paths where it's configured for use.
        if ($enabled_or_disabled === 'enabled' && !in_array($current_path, $configured_urls)) {
          return;
        }

        // Dont use hide submit on the configured paths.
        if ($enabled_or_disabled === 'disabled' && in_array($current_path, $configured_urls)) {
          return;
        }
      }
    }

    $this->hideSubmitAttachJsCss($form);
    $form['#attached']['drupalSettings']['hide_submit'] = [
      'method' => $config->get('hide_submit_method'),
      'reset_time' => (int) $config->get('hide_submit_reset_time'),
      'abtext' => $config->get('hide_submit_abtext'),
      'atext' => $config->get('hide_submit_atext'),
      'hide_fx' => $config->get('hide_submit_hide_fx'),
      'hide_text' => $config->get('hide_submit_hide_text'),
      'indicator_style' => $config->get('hide_submit_indicator_style'),
      'spinner_color' => $config->get('hide_submit_spinner_color'),
      'spinner_lines' => (int) $config->get('hide_submit_spinner_lines'),
    ];
  }

  /**
   * Attaches hide submit Javascript and CSS libraries to forms.
   *
   * @param array $form
   *   The form that requires additional assets.
   */
  protected function hideSubmitAttachJsCss(&$form) {
    $form['#attached']['library'][] = 'hide_submit/hide_submit';

    $config = $this->configFactory->get('hide_submit.settings');
    if ($config->get('hide_submit_method') === 'indicator') {
      $form['#attached']['library'][] = 'hide_submit/spin';
      $form['#attached']['library'][] = 'hide_submit/ladda';
    }
    if ($config->get('hide_submit_method') === 'disable' || $config->get('hide_submit_method') === 'hide') {
      $form['#attached']['library'][] = 'hide_submit/hide_submit';
    }
  }

}
